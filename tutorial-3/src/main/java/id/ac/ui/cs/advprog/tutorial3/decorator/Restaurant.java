package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Restaurant {
    public static void main(String[] args) {
        Food thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();
        System.out.print(thickBunBurgerSpecial.getDescription() + " "
                + thickBunBurgerSpecial.cost());
        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(thickBunBurgerSpecial);
        System.out.print(thickBunBurgerSpecial.getDescription() + " "
                + thickBunBurgerSpecial.cost());
    }
}
