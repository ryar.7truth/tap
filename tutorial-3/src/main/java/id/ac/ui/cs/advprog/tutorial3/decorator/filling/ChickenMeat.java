package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChickenMeat extends Filling {
    Food food;

    public ChickenMeat(Food food) {
        //TODO Implement
        this.food =  food;
        this.description = food.getDescription() + ", adding chicken meat";
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return this.description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return  this.food.cost() + 4.50;
    }
}
